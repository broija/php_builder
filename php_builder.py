#!/usr/local/bin/python3.7
# coding: utf-8

import argparse
import configparser
import os
import pathlib
import shutil

import broijalib.archive
import broijalib.compilation

# ------------------------------------------------------------------------------

_script_dir = pathlib.Path(__file__).resolve().parent

argparser = argparse.ArgumentParser(description='PHP builder.')

argparser.add_argument('archive', type=pathlib.Path, help='Path to PHP archive or source path.')
argparser.add_argument('--config', type=pathlib.Path, default=(_script_dir / 'config.ini'), help='Path to build configuration file.')
argparser.add_argument('--build-parent', type=pathlib.Path, default='/data/prog/php/', help='Path to config/build parent dir.')
argparser.add_argument('--prefix-parent', type=pathlib.Path, default='/data/prog/php/install/', help='Path to installation dir.')
argparser.add_argument('--dry-run','-s', default=False, action='store_true', help='Simulation / dry run.')

args = argparser.parse_args()

# Checking source archive existence
args.archive = args.archive.resolve(strict=True)

# Checking configuration file existence
args.config = args.config.resolve(strict=True)

if not args.prefix_parent.is_dir():
  os.makedirs(args.prefix_parent)

if not args.build_parent:
  os.makedirs(args.build_parent)

suffixes = args.archive.suffixes
suffixes.reverse()

basename = broijalib.archive.strip_exts(args.archive)

main_build_path = args.build_parent / basename  
source_path = main_build_path / 'src'

if not source_path.is_dir():
  shutil.unpack_archive(args.archive,source_path)

builder = broijalib.compilation.Builder(log_dir_path=main_build_path, dryrun=args.dry_run)

config = configparser.ConfigParser()
config.read(args.config)

sections = config.sections()

for section in sections:

  build_path = main_build_path / section

  cpp = None

  if 'c-preprocessor' in config[section]:
    cpp = config[section]['c-preprocessor']

  cc = None

  if 'c-compiler' in config[section]:
    cc = config[section]['c-compiler']

  cxx = None

  if 'c++-compiler' in config[section]:
    cxx = config[section]['c++-compiler']

  build_options = None

  if 'options' in config[section]:
    build_options = config[section]['options']

  build_prefix = args.prefix_parent / basename / config[section]['installdir']
  
  builder.configure(source_path, build_path, cpp=cpp, cc=cc, cxx=cxx, options=build_options, prefix=build_prefix)

  builder.make(jobs=2)
  builder.test(env = {**os.environ, 'TESTS': '-q'})
  builder.install()
